#include "FFTBottomSolver.h"

template <class Level>
FFTBottomSolver<Level>::FFTBottomSolver(Mesh_t *mesh,
                                        FieldLayout_t *fl,
                                        std::string greensFunction)
    : FFTPoissonSolver(mesh, fl, greensFunction, "OPEN")
{
    for (lo_t i = 0; i < 2 * AMREX_SPACEDIM; ++i) {
        if (Ippl::getNodes()>1)
            bc_m[i] = new ParallelInterpolationFace<double, AMREX_SPACEDIM, Mesh_t, Center_t>(i);
        else
            bc_m[i] = new InterpolationFace<double, AMREX_SPACEDIM, Mesh_t, Center_t>(i);
    }
    
    rho_m.initialize(*mesh,
                     *fl,
                     GuardCellSizes<AMREX_SPACEDIM>(1),
                     bc_m);
}


template <class Level>
Mesh_t* FFTBottomSolver<Level>::initMesh(AmrOpal* amrobject_p)
{
    if ( amrobject_p == nullptr )
        throw OpalException("FFTBottomSolver::initMesh()",
                            "No AMR object initialized.");
    
    // find the number of grid points in each dimension
    const AmrGeometry_t& geom = amrobject_p->Geom(0);
    const AmrBox_t& bx = geom.Domain();
    AmrIntVect_t iv = bx.size();
    
    NDIndex<AMREX_SPACEDIM> domain;
    domain[0] = Index(0, iv[0]);
    domain[1] = Index(0, iv[1]);
#if AMREX_SPACEDIM == 3
    domain[2] = Index(0, iv[2]);
#endif
    return new Mesh_t(domain);
}


template <class Level>
FieldLayout_t* FFTBottomSolver<Level>::initFieldLayout(Mesh_t *mesh,
                                                       AmrOpal* amrobject_p)
{
    if ( amrobject_p == nullptr )
        throw OpalException("FFTBottomSolver::initFieldLayout()",
                            "No AMR object initialized.");
    
    const AmrGrid_t& ba = amrobject_p->boxArray(0);
    const AmrProcMap_t& dmap = amrobject_p->DistributionMap(0);
    auto pmap = dmap.ProcessorMap();
    
    std::vector< NDIndex<AMREX_SPACEDIM> > regions;
    std::vector< int > nodes;
    for (uint i = 0; i < pmap.size(); ++i) {
        AmrBox_t bx = ba[i];
        
        NDIndex<AMREX_SPACEDIM> range;
        for (int j = 0; j < AMREX_SPACEDIM; ++j)
            range[j] = Index(bx.smallEnd(j), bx.bigEnd(j));
        
        regions.push_back( range );
        nodes.push_back( pmap[i] );
    }
    
    return new FieldLayout_t(*mesh,
                             &regions[0],
                             &regions[0] + regions.size(),
                             &nodes[0],
                             &nodes[0] + nodes.size());
}


template <class Level>
void FFTBottomSolver<Level>::solve(const Teuchos::RCP<mv_t>& x,
                            const Teuchos::RCP<mv_t>& b )
{
    Vector_t hr;
    
    hr[0] = level_mp->cellSize(0);
    hr[1] = level_mp->cellSize(1);
#if AMREX_SPACEDIM == 3
    hr[2] = level_mp->cellSize(2);
#endif
    this->vector2field_m(b);
    
    FFTPoissonSolver::computePotential(rho_m, hr);
    
    this->field2vector_m(x);
}


template <class Level>
void FFTBottomSolver<Level>::setOperator(const Teuchos::RCP<matrix_t>& A,
                                          Level* level_p)
{
    level_mp = level_p;
    
    // do nothing here
    this->fillMap_m(level_p);
    
};


template <class Level>
void FFTBottomSolver<Level>::fillMap_m(Level* level_p)
{
    map_m.clear();
    
    for (amrex::MFIter mfi(level_p->grids, level_p->dmap, true); mfi.isValid(); ++mfi) {
        const amrex::Box&       tbx = mfi.tilebox();
        const int* lo = tbx.loVect();
        const int* hi = tbx.hiVect();
        
        for (int i = lo[0]; i <= hi[0]; ++i) {
            for (int j = lo[1]; j <= hi[1]; ++j) {
#if AMREX_SPACEDIM == 3
                for (int k = lo[2]; k <= hi[2]; ++k) {
#endif
                    AmrIntVect_t iv(D_DECL(i, j, k));
                    
                    go_t gidx = level_p->serialize(iv);
                    lo_t lidx = level_p->map_p->getLocalElement(gidx);
                    
                    map_m[lidx] = iv;
#if AMREX_SPACEDIM == 3
                }
#endif
            }
        }
    }
}


template <class Level>
void FFTBottomSolver<Level>::field2vector_m(const Teuchos::RCP<mv_t>& vector)
{
    for (size_t i = 0; i < vector->getLocalLength(); ++i) {
        AmrIntVect_t iv = map_m[i];
#if AMREX_SPACEDIM == 3
        vector->replaceLocalValue(i, 0, rho_m[iv[0]][iv[1]][iv[2]].get());
#else
        vector->replaceLocalValue(i, 0, rho_m[iv[0]][iv[1]].get());
#endif
    }
}
    

template <class Level>
void FFTBottomSolver<Level>::vector2field_m(const Teuchos::RCP<mv_t>& vector)
{
    for (size_t i = 0; i < vector->getLocalLength(); ++i) {
        AmrIntVect_t iv = map_m[i];
#if AMREX_SPACEDIM == 3
        rho_m[iv[0]][iv[1]][iv[2]] = vector->getData(0)[i];
#else
        rho_m[iv[0]][iv[1]] = vector->getData(0)[i];
#endif
    }
}
